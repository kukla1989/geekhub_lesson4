# README
lesson 4 
1. Створити новий проект.
2. Створити таблицю Article з наступними полями: title з типом string, content з типом text.
3. На посиланні localhost:3000/articles, повинна бути можливість переглядати всі записи з таблиці Article. Також на цій сторінці повинна бути можливість створювати, видаляти, редагувати та переглядати записи які належать до таблиці Article.
4. Створити таблицю Comment з наступними полями: author:string body:text.
5. За допомогою has_many i belongs_to створити зв"язки між цими таблицями.
6. На посиланні localhost:3000/articles/тут_id_article, повинна бути можливість переглядати всі коменти і додавання нових коментів які будуть належати до конкретного Article.
This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
