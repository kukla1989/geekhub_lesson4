Rails.application.routes.draw do
  resources :comments
  resources :articles
  root 'articles#index'
  get '/index_article_comments/:id', to: 'comments#index_article_comments'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
